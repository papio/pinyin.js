#pinyin.js
 **说明：** 
本项目主要通过js将汉字转换为拼音，适用于所有web前台，使用时只需将pinyingUtil.js引入并调用相应功能函数即可。
 **使用示范：** 
```
<script type="text/javascript" src="$cxt/static/js/pinyinUtil.js"></script>
<script type="text/javascript">
    //将汉字转为拼音，首字母大写
    var pinyin = chineseToPinyin('我爱中国');//输出WoAiZhongGuo
    //获取汉字首字母
    var pinyin = pinyinFirst('我爱中国');//输出WAZG
</script>
```
 **缺陷：** 
目前此js无法辨识多音字，例如会将“银行”输出为“YinXing”。
